import java.util.ArrayList;

public class MedicineList {
    private Medicine medicines;
    private Medicine currentMed;
    public ArrayList<Medicine> medications = new ArrayList<>();

    public MedicineList(){

    }
    public void addMedicine(Medicine medicine){
        medications.add(medicine);
    }

    public void write(){
        if(medications.size() > 0){
            System.out.format("%s\n", "=".repeat(105));
            System.out.println("Medicijnlijst");
            for(Medicine medicine:medications){
                medicine.write();
            }
        } else {
            System.out.println("Deze patiënt heeft op dit moment geen medicijnen");
        }

    }

    public void editMedicineData(Medicine medicine){medicine.editMedicine();}

    //Shows medlist of current patient and let you select one of them, so you can edit the dosis
    public void writeWithNumbers(){
        for(Medicine medicine:medications){
            System.out.println(medicine.getId() + " " + medicine.getName() + " " + medicine.getDosis());
        }
        System.out.print("Voer #keuze in:");
        var scanner = new BScanner(System.in);
        int med = scanner.nextInt();
        med--;
        if(med > medications.size() - 1){
            System.out.println( "Voer een *geldig* cijfer in" );
            writeWithNumbers();
        } else {
            //System.out.println("Here I am!!");
            medicines = medications.get(med);
            currentMed = medicines;
            System.out.format( "%s\n", "=".repeat( 105 ) );
            System.out.println(currentMed.getName() + " " + currentMed.getDosis());
            editMedicineData(currentMed);
        }
    }
    //Shows general medlist and let you select one of them to put in de patient medlist.
    public void showFullMedlist(Patient patient){
        System.out.println("Beschikbare medicijnen : ");
        for(Medicine medicine:medications){
            System.out.println(medicine.getId() + " " + medicine.getName() + " " + medicine.getDosis());
        }
        System.out.print("Voer #keuze in:");
        var scanner = new BScanner(System.in);
        int med = scanner.nextInt();
        med--;
        if(med > medications.size() - 1){
            System.out.println( "Voer een *geldig* cijfer in" );
            showFullMedlist(patient);
        } else {
            //System.out.println("Here I am!!");
            medicines = medications.get(med);
            currentMed = medicines;
            System.out.format( "%s\n", "=".repeat( 105 ) );
            System.out.println(currentMed.getName() + " is toegevoegd aan de medicijnen lijst van " + patient.getFirstName() + " " + patient.getSurname());
            patient.addMedicine(currentMed);
        }
    }

}
