import java.time.LocalDate;

public class LungContent {
    private double lungContentNumber;
    private LocalDate date;
    private int starLength;
    public LungContent(LocalDate d, double lc){
        this.date                 = d;
        this.lungContentNumber    = lc;
    }

    public void printWithStars(LocalDate d, double lc){
        starLength = (int) lc;
        System.out.format( "[ %-5s ] %-10s  (%s)\n", d,  "*".repeat(starLength) , lc);
    }

    public void printWithStarsOneStarLes(LocalDate d, double lc, int sl){
        starLength = sl;
        starLength--;
        System.out.format( "[ %-5s ] %-10s  (%s)\n", d,  "*".repeat(starLength) , lc);
    }
    //If weight is less then last time and if it is between 1 and 9 kg then you do this function
    public void printWithStarsTwoStarLes(LocalDate d, double lc, int sl){
        starLength = sl - 2;
        System.out.format( "[ %-5s ] %-10s  (%s)\n", d,  "*".repeat(starLength) , lc);
    }
    //If weight is less then last time and if it is higher 10 you do this function
    public void printWithStarsThreeStarLes(LocalDate d, double lc, int sl){
        starLength = sl - 3;
        System.out.format( "[ %-5s ] %-10s  (%s)\n", d,  "*".repeat(starLength) , lc);
    }
    //If weight is less then last time and if it is between 0.01 and 0.99 then you do this function
    public void printWithStarsOneStarMore(LocalDate d, double lc, int sl){
        starLength = sl;
        starLength++;
        System.out.format( "[ %-5s ] %-10s  (%s)\n", d,  "*".repeat(starLength) , lc);
    }
    //If weight is less then last time and if it is between 1 and 9 kg then you do this function
    public void printWithStarsTwoStarMore(LocalDate d, double lc, int sl){
        starLength = sl + 2;
        System.out.format( "[ %-5s ] %-10s  (%s)\n", d,  "*".repeat(starLength) , lc);
    }


    //If weight is less then last time and if it is higher 10 you do this function
    public void printWithStarsThreeStarMore(LocalDate d, double lc, int sl){
        starLength = sl + 3;
        System.out.format( "[ %-5s ] %-10s  (%s)\n", d,  "*".repeat(starLength) , lc);
    }

    public LocalDate getDate() {
        return date;
    }
    public int getStarLength() {
        return starLength;
    }

    public double getLungContentNumber() {
        return lungContentNumber;
    }
}
