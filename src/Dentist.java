public class Dentist extends User{
    public Dentist(int id, String name, String discipline) {
        super(id, name, discipline);
    }
    @Override
    public String getDiscipline(){
        return "Tandarts";
    }

    @Override
    public boolean hasAccessMedicationEdit() {
        return true;
    }

    @Override
    public boolean hasAccesBMI() {
        return false;
    }
}
