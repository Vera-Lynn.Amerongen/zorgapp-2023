public abstract class User
{
   private String userName;
   private int    userID;
   private String discipline;

   public abstract String getDiscipline();
   public abstract boolean hasAccessMedicationEdit();
   public abstract boolean hasAccesBMI();

   ///////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////
   public String getUserName()
   {
      return userName;
   }

   ///////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////
   public int getUserID()
   {
      return userID;
   }

   ///////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////
   public User( int id, String name, String discipline )
   {
      this.userID       = id;
      this.userName     = name;
      this.discipline   = discipline;
   }
   public void editPatientData(Patient patient){patient.editData(patient);}
}
