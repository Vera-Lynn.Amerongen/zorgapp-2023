import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;


///////////////////////////////////////////////////////////////////////////
// class Administration represents the core of the application by showing
// the main menu, from where all other functionality is accessible, either
// directly or via sub-menus.
//
// An Adminiatration instance needs a User as input, which is passed via the
// constructor to the data member 'çurrentUser'.
// The patient data is available via the data member çurrentPatient.
/////////////////////////////////////////////////////////////////
public class Administration
{
   static final int STOP = 0;
   static final int VIEW = 1;
   static final int EDIT = 2;
   static final int CHANGE = 3;
   static final int SHOWPATIENTS = 4;
   static final int VIEWGENERALMEDLIST = 5;
   static final int VIEWGENERALCONSULTLIST = 6;
   static final int ADDCONSULT = 7;
   static final int EDITMEDLIST = 8;
   static final int ADDMEDICINE  = 9;

   static final int SORTAZ = 1;
   static final int SORTZA = 2;
   static final int SORT110 = 3;
   static final int SORT101 = 4;


   private Patient[] patients;
   private Patient patient;
   private Patient currentPatient;// The currently selected patient
   private int curPatient;
   private User    currentUser;               // the current user of the program.
   private MedicineList allMedlist = new MedicineList();
   private ConsultList dentistConsults = new ConsultList();
   private ConsultList doctorConsults = new ConsultList();
   private ConsultList physioConsults = new ConsultList();

   /////////////////////////////////////////////////////////////////
   // Constructor
   /////////////////////////////////////////////////////////////////
   public Administration( User user )
   {
      currentUser    = user;
      //currentPatient = new Patient( 1, "Van Puffelen", "Pierre", LocalDate.of( 2000, 2, 29 ), 80.5,1.67 );
      Patient patient1 = new Patient( 1, "Van Puffelen", "Pierre", LocalDate.of( 2000, 2, 29 ), 88.1, 1.67);
      patient1.addMedicine(new Medicine(1, "Vitamine D", "Tablet", "1 per dag"));
      patient1.addMedicine(new Medicine(2, "Ibruprofen", "Tablet", "max 12 per dag van 200 mg"));

      patient1.addWeight(new Weight(88.1));
      patient1.addWeight(new Weight(80.1));

      patient1.addConsultToPatient(new Consult(1, "Tandarts", "DEFAULT","Routine controle", 20.00));
      patient1.addConsultToPatient(new Consult(2, "Fysiotherapeut", "DEFAULT","Standaard behandeling", 17.50));

      patient1.addLungContentToPatient(new LungContent(LocalDate.of(2023, 3, 25),7.0));
      patient1.addLungContentToPatient(new LungContent(LocalDate.of(2023, 3, 26),5.0));
      patient1.addLungContentToPatient(new LungContent(LocalDate.of(2023, 3, 27),6.0));
      patient1.addLungContentToPatient(new LungContent(LocalDate.of(2023, 3, 28),6.0));

      Patient patient2 = new Patient( 2, "Van Weesp", "Flynn", LocalDate.of( 2004, 7, 29 ), 80.1, 1.70);
      patient2.addWeight(new Weight(80.1));

      Patient patient3 = new Patient( 3, "Blauw", "Finn", LocalDate.of( 2004, 8, 7 ), 80.1, 1.70);
      patient3.addWeight(new Weight(80.1));

      Patient patient4 = new Patient( 4, "Groeneweg", "Lynn", LocalDate.of( 2005, 7, 7 ), 70.1, 1.70);
      patient4.addWeight(new Weight(70.1));

      Patient patient5 = new Patient( 5, "Koslov", "Jesper", LocalDate.of( 2007, 10, 10 ), 70.1, 1.75);
      patient5.addWeight(new Weight(70.1));

      Patient patient6 = new Patient(6, "Koslov", "Vladimirus", LocalDate.of( 2000, 7, 3 ), 70.1, 1.75);


      patients = new Patient[]{patient1,patient2,patient3,patient4,patient5,patient6};
      allMedlist.addMedicine(new Medicine(1, "Vitamine D", "Tablet", "1 per dag"));
      allMedlist.addMedicine(new Medicine(2, "Ibruprofen", "Tablet", "max 12 per dag van 200 mg"));
      allMedlist.addMedicine(new Medicine(3, "Methylfenidaat", "Tablet", "max 6 pillen per dag"));
      allMedlist.addMedicine(new Medicine(4, "Macrogol", "Poeder", "Een zakje per dag"));
      allMedlist.addMedicine(new Medicine(5, "Insuline", "Injectie", "Dosis bepalen aan de hand van eten en drinken"));


      dentistConsults.addConsult(new Consult(1, "Tandarts", "DEFAULT","Routine controle", 20.00));
      dentistConsults.addConsult(new Consult(2, "Tandarts", "SIMPLE","Extractie", 30.00));
      dentistConsults.addConsult(new Consult(3, "Tandarts", "SIMPLE","Fluoridebehandeling", 30.00));
      dentistConsults.addConsult(new Consult(4, "Tandarts", "COMPLEX","Wortelkanaalbehandeling", 55.00));
      dentistConsults.addConsult(new Consult(5, "Tandarts", "COMPLEX","Implantaat", 55.00));

      physioConsults.addConsult(new Consult(1, "Fysiotherapeut", "DEFAULT","Standaard behandeling", 17.50));
      physioConsults.addConsult(new Consult(2, "Fysiotherapeut", "SHORT","Tapen en bandageren", 25.00));
      physioConsults.addConsult(new Consult(3, "Fysiotherapeut", "SHORT","Mobilisatie", 25.00));
      physioConsults.addConsult(new Consult(4, "Fysiotherapeut", "SHORT","Massage", 25.00));
      physioConsults.addConsult(new Consult(5, "Fysiotherapeut", "EXTENDED","Manuele therapie", 50.00));
      physioConsults.addConsult(new Consult(6, "Fysiotherapeut", "EXTENDED","Dry needling", 50.00));
      physioConsults.addConsult(new Consult(7, "Fysiotherapeut", "FACILITIES","Gebruiken van oefenblad", 5.00));

      doctorConsults.addConsult(new Consult(1, "Huisarts", "DEFAULT","Consult", 21.50));
      doctorConsults.addConsult(new Consult(2, "Huisarts", "EXTENDED","Huisbezoek", 43.00));
      doctorConsults.addConsult(new Consult(3, "Huisarts", "EXTENDED","Gezondheidsonderzoek", 43.00));

      checkInputPatientInt(currentUser);
   }
   public void wait(int s)
   {
      try
      {
         TimeUnit.SECONDS.sleep(s);
      }
      catch(InterruptedException ex)
      {
         Thread.currentThread().interrupt();
      }
   }

   /////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////
   public void menu(User user)
   {
      var scanner = new BScanner( System.in );  // User input via this scanner.

      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.format( "%s\n", "=".repeat( 105 ) );
         System.out.format( "Huidige gebruiker: [%d] %s\n", user.getUserID(), user.getUserName() );
         System.out.format( "%s\n", "=".repeat( 105 ) );
         System.out.format( "Huidige patiënt: %s\n", currentPatient.fullName() );
         System.out.format( "%s\n", "=".repeat( 105 ) );

         ////////////////////////a
         // Print menu on screen
         ////////////////////////
         System.out.format( "%d:  Ga terug naar begin menu\n", STOP );
         System.out.format( "%d:  Patiëntgegevens bekijken van huidige patiënt\n", VIEW );
         System.out.format( "%d:  Patiëntgegevens bewerken van huidige patiënt\n", EDIT );
         System.out.format( "%d:  Verander de huidige patiënt\n", CHANGE );
         System.out.format( "%d:  Bekijk huidige patiëntenlijst\n", SHOWPATIENTS );
         System.out.format( "%d:  Bekijk de lijst met alle medicijnen\n", VIEWGENERALMEDLIST );
         System.out.format( "%d:  Bekijk de lijst met alle consulten\n", VIEWGENERALCONSULTLIST);
         System.out.format( "%d:  Consult toevoegen aan de huidige patiënt\n", ADDCONSULT);
         if(user.hasAccessMedicationEdit()){
            System.out.format( "%d:  Medicijn lijst van de huidige patiënt bewerken\n", EDITMEDLIST);
            System.out.format( "%d:  Medicijnen toevoegen aan de huidige patiënt\n", ADDMEDICINE);
         }



         ////////////////////////

         System.out.print( "Voer #keuze in: " );
         int choice = scanner.nextInt();
         switch (choice)
         {
            case STOP: // interrupt the loop
               nextCycle = false;
               break;

            case VIEW:
               currentPatient.viewData(currentUser);
               if(currentPatient.getMedicineList().medications.size() > 0){
                  currentPatient.writeMedicine();
               } else{
                  System.out.println("Deze patiënt gebruikt op het moment geen medicijnen");
               }
               if (currentPatient.getLungContentList().lungContentArrayList.size() > 0){
                  System.out.format("%s\n", "-".repeat(105));
                  System.out.println("Long inhoud overzicht:");
                  System.out.format("%s\n", "-".repeat(105));
                  currentPatient.lungContentWrite();
               } else{
                  System.out.println("Deze patiënt heeft op dit moment geen metingen gehad voor de long inhoud");
               }
               if(currentPatient.getConsultList().consults.size() > 0){
                  currentPatient.consultWrite();
               } else {
                  System.out.println("Deze patiënt heeft op dit moment geen consulten");
               }

               wait(10);
               break;

            case EDIT:
               //var editScanner = new BScanner(System.in);
               System.out.format( "%s\n", "=".repeat( 105 ) );
               System.out.println(currentPatient.fullName());
               currentUser.editPatientData(currentPatient);
               break;

            case CHANGE:
               checkInputPatientInt(currentUser);

               break;

            case SHOWPATIENTS:
               showPatients();
               break;

            case VIEWGENERALMEDLIST:
               allMedlist.write();
               wait(10);
               break;

            case VIEWGENERALCONSULTLIST:
               if(currentUser.getDiscipline() == "Dokter"){
                  doctorConsults.write();
                  wait(10);
               } else if (currentUser.getDiscipline() == "Tandarts") {
                  dentistConsults.write();
                  wait(10);
               } else {
                  physioConsults.write();
                  wait(10);
               }
               break;

            case ADDCONSULT:
               if(currentUser.getDiscipline() == "Dokter"){
                   doctorConsults.writeAdd(currentPatient);
                   wait(5);
               } else if (currentUser.getDiscipline() == "Tandarts") {
                  dentistConsults.writeAdd(currentPatient);
                  wait(5);
               } else {
                  physioConsults.writeAdd(currentPatient);
                  wait(5);
               }
               break;

            case EDITMEDLIST:
               if(user.hasAccessMedicationEdit()){
                  currentPatient.medicineWrite();
               }
               else{ System.out.println( "Voer een *geldig* cijfer in" );}
               wait(5);
               break;

            case ADDMEDICINE:
               if(user.hasAccessMedicationEdit()){
                  allMedlist.showFullMedlist(currentPatient);
               }
               else{ System.out.println( "Voer een *geldig* cijfer in" );}
               wait(5);
               break;



            default:
               System.out.println( "Voer een *geldig* cijfer in" );
               wait(5);
               break;
         }
      }
   }

   public void checkInputPatientInt(User user){
      System.out.format( "%s\n", "=".repeat( 105 ) );
      System.out.format( "Huidige gebruiker: [%d] %s\n", user.getUserID(), user.getUserName() );
      System.out.format( "%s\n", "=".repeat( 105 ) );
      System.out.println( "Selecteer uw patient: " );
      var changeScanner = new BScanner(System.in);
      System.out.format( "%s\n", "=".repeat( 105 ) );
      for (int i = 0; i < patients.length; i++) {
         patient = patients[i];
         System.out.println(patient.getId() + " " + patient.fullName());
      }
      System.out.print( "Voer patiënt ID in: " );
      int patr = changeScanner.nextInt();
      //System.out.println("Here I am!!");
      patr--;
      if(patr > patients.length - 1){
         System.out.println( "Voer een *geldig* cijfer in" );
         checkInputPatientInt(currentUser);
      } else {
         //System.out.println("Here I am!!");
         patient = patients[patr];
         currentPatient = patient;
      }
   }

   public void showPatients(){
      sortPatients();
   }


   public void sortPatients(){
      System.out.format( "%d:  Sorteer achternaam A-Z\n", SORTAZ );
      System.out.format( "%d:  Sorteer achternaam Z-A\n", SORTZA );
      System.out.format( "%d:  Sorteer geboorte datum 1-10\n", SORT110 );
      System.out.format( "%d:  Sorteer geboorte datum 10-1\n", SORT101 );
      var answerScanner = new BScanner(System.in);
      System.out.print( "Voer u keuze in:" );
      int choice2 = answerScanner.nextInt();
      switch (choice2){
         case SORTAZ:
            TreeSet<Patient> gfg
                    = new TreeSet<>(new Patient.FirstComparator());

            gfg.add(new Patient( 1, "Van Puffelen", "Pierre", LocalDate.of( 2000, 2, 29 ), 88.1, 1.67));
            gfg.add(new Patient( 2, "Van Weesp", "Flynn", LocalDate.of( 2004, 7, 29 ), 80.1, 1.70));
            gfg.add(new Patient( 3, "Blauw", "Finn", LocalDate.of( 2004, 8, 7 ), 80.1, 1.70));
            gfg.add(new Patient( 4, "Groeneweg", "Lynn", LocalDate.of( 2005, 7, 7 ), 70.1, 1.70));
            gfg.add(new Patient(5, "Koslov", "Jesper", LocalDate.of( 2007, 10, 10 ), 70.1, 1.75));
            gfg.add(new Patient(6, "Koslov", "Vladimirus", LocalDate.of( 2000, 3, 3 ), 70.1, 1.75));

            // printing each employee object
            System.out.format( "%s\n", "=".repeat( 105 ) );
            System.out.println("Lijst met patiënten gesorteerd op achternaam van A naar Z:");
            System.out.println();
            for (Patient patient : gfg) {
               System.out.println(patient.fullName());
               System.out.format( "%s\n", "-".repeat( 105 ) );
            }
            wait(10);
            break;
         case SORTZA:
            TreeSet<Patient> patientTreeSet
                    = new TreeSet<>(new Patient.SecondComparator());

            patientTreeSet.add(new Patient( 1, "Van Puffelen", "Pierre", LocalDate.of( 2000, 2, 29 ), 88.1, 1.67));
            patientTreeSet.add(new Patient( 2, "Van Weesp", "Flynn", LocalDate.of( 2004, 7, 29 ), 80.1, 1.70));
            patientTreeSet.add(new Patient( 3, "Blauw", "Finn", LocalDate.of( 2004, 8, 7 ), 80.1, 1.70));
            patientTreeSet.add(new Patient( 4, "Groeneweg", "Lynn", LocalDate.of( 2005, 7, 7 ), 70.1, 1.70));
            patientTreeSet.add(new Patient(5, "Koslov", "Jesper", LocalDate.of( 2007, 10, 10 ), 70.1, 1.75));
            patientTreeSet.add(new Patient(6, "Koslov", "Vladimirus", LocalDate.of( 2000, 3, 3 ), 70.1, 1.75));

            // printing each employee object
            System.out.format( "%s\n", "=".repeat( 105 ) );
            System.out.println("Lijst met patiënten gesorteerd op achternaam van Z naar A:");
            System.out.println();
            for (Patient patient : patientTreeSet) {
               System.out.println(patient.fullName());
               System.out.format( "%s\n", "-".repeat( 105 ) );
            }
            wait(10);
            break;
         case SORT110:
            TreeSet<Patient> patientTreeSet1
                    = new TreeSet<>(new Patient.ThirdComparator());

            patientTreeSet1.add(new Patient(1, "Koslov", "Vladimirus", LocalDate.of( 2000, 3, 3 ), 70.1, 1.75));
            patientTreeSet1.add(new Patient( 2, "Van Puffelen", "Pierre", LocalDate.of( 2000, 2, 29 ), 88.1, 1.67));
            patientTreeSet1.add(new Patient( 3, "Van Weesp", "Flynn", LocalDate.of( 2004, 7, 29 ), 80.1, 1.70));
            patientTreeSet1.add(new Patient( 4, "Blauw", "Finn", LocalDate.of( 2004, 8, 7 ), 80.1, 1.70));
            patientTreeSet1.add(new Patient( 5, "Groeneweg", "Lynn", LocalDate.of( 2005, 7, 7 ), 70.1, 1.70));
            patientTreeSet1.add(new Patient(6, "Koslov", "Jesper", LocalDate.of( 2007, 10, 10 ), 70.1, 1.75));


            // printing each employee object
            System.out.format( "%s\n", "=".repeat( 105 ) );
            System.out.println("Lijst met patiënten gesorteerd van oudste naar jongste:");
            System.out.println();
            for (Patient patient : patientTreeSet1) {
               System.out.println(patient.fullName());
               System.out.format( "%s\n", "-".repeat( 105 ) );
            }
            wait(10);
            break;
         case SORT101:
            TreeSet<Patient> patientTreeSet2
                    = new TreeSet<>(new Patient.FourthComparator());

            patientTreeSet2.add(new Patient( 1, "Van Puffelen", "Pierre", LocalDate.of( 2000, 2, 29 ), 88.1, 1.67));
            patientTreeSet2.add(new Patient( 2, "Van Weesp", "Flynn", LocalDate.of( 2004, 7, 29 ), 80.1, 1.70));
            patientTreeSet2.add(new Patient( 3, "Blauw", "Finn", LocalDate.of( 2004, 8, 7 ), 80.1, 1.70));
            patientTreeSet2.add(new Patient( 4, "Groeneweg", "Lynn", LocalDate.of( 2005, 7, 7 ), 70.1, 1.70));
            patientTreeSet2.add(new Patient(5, "Koslov", "Jesper", LocalDate.of( 2007, 10, 10 ), 70.1, 1.75));
            patientTreeSet2.add(new Patient(6, "Koslov", "Vladimirus", LocalDate.of( 2000, 3, 3 ), 70.1, 1.75));

            // printing each employee object
            System.out.format( "%s\n", "=".repeat( 105 ) );
            System.out.println("Lijst met patiënten gesorteerd van jongste naar oudste:");
            System.out.println();
            for (Patient patient : patientTreeSet2) {
               System.out.println(patient.fullName());
               System.out.format( "%s\n", "-".repeat( 105 ) );
            }
            wait(10);
      }

   }
}
