public class PhysioTherapist extends User{
    public PhysioTherapist(int id, String name, String discipline) {
        super(id, name, discipline);
    }
    @Override
    public String getDiscipline(){
        return "Fysiotherapeut";
    }

    @Override
    public boolean hasAccessMedicationEdit() {
        return false;
    }

    @Override
    public boolean hasAccesBMI() {
        return true;
    }
}
