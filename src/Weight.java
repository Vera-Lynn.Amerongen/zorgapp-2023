public class Weight {
    private double weight;

    public void setWeight(double w) {
        this.weight = w;
    }

    public double getWeight() {
        return weight;
    }

    public Weight(double w){
        this.weight = w;
    }

    public void write(){
        System.out.format( "%-17s %s\n", "Gewicht:", weight + " kg");
    }

    public void print(double w){
        System.out.format( "%-5s %s\n", "Gewicht:", w + " kg");
    }

    public void printWithStars(double w){
        int intValue = (int) w;
        int starWeight = intValue / 10;
        System.out.format( "%-1s %s\n", w + " kg |", "*".repeat(starWeight));
    }
    //If weight is less then last time and if it is between 0.01 and 0.99 then you do this function
    public void printWithStarsOneStarLes(double w){
        int intValue = (int) w;
        int starWeight = (intValue - 10) / 10;
        System.out.format( "%-1s %s\n", w + " kg |", "*".repeat(starWeight));
    }
    //If weight is less then last time and if it is between 1 and 9 kg then you do this function
    public void printWithStarsTwoStarLes(double w){
        int intValue = (int) w;
        int starWeight = (intValue - 20) / 10;
        System.out.format( "%-1s %s\n", w + " kg |", "*".repeat(starWeight));
    }
    //If weight is less then last time and if it is higher 10 you do this function
    public void printWithStarsThreeStarLes(double w){
        int intValue = (int) w;
        int starWeight = (intValue - 30) / 10;
        System.out.format( "%-1s %s\n", w + " kg |", "*".repeat(starWeight));
    }
    //If weight is less then last time and if it is between 0.01 and 0.99 then you do this function
    public void printWithStarsOneStarMore(double w){
        int intValue = (int) w;
        int starWeight = (intValue + 10) / 10;
        System.out.format( "%-1s %s\n", w + " kg |", "*".repeat(starWeight));
    }
    //If weight is less then last time and if it is between 1 and 9 kg then you do this function
    public void printWithStarsTwoStarMore(double w){
        int intValue = (int) w;
        int starWeight = (intValue + 20) / 10;
        System.out.format( "%-1s %s\n", w + " kg |", "*".repeat(starWeight));
    }
    //If weight is less then last time and if it is higher 10 you do this function
    public void printWithStarsThreeStarMore(double w){
        int intValue = (int) w;
        int starWeight = (intValue + 30) / 10;
        System.out.format( "%-1s %s\n", w + " kg  |", "*".repeat(starWeight));
    }
}
