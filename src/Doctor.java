public class Doctor extends User{

    public Doctor(int id, String name, String discipline) {
        super(id, name, discipline);
    }

    @Override
    public String getDiscipline(){
        return "Dokter";
    }

    @Override
    public boolean hasAccessMedicationEdit() {
        return true;
    }

    @Override
    public boolean hasAccesBMI() {
        return true;
    }
}
