import java.util.ArrayList;

public class ConsultList {
    public ArrayList<Consult> consults = new ArrayList<>();
    public Consult consult;
    public Consult currentConsult;

    public ConsultList(){

     }
    public void addConsult(Consult consult){
        consults.add(consult);
    }

    public void write(){
        System.out.format("%s\n", "=".repeat(105));
        System.out.printf("| %-15s | %-15s | %-30s | %-12s |%n", "Specialism", "Rate Category", "Rate type", "Rate");
        System.out.format("%s\n", "=".repeat(105));
        for(Consult consult:consults){
            consult.write();
        }
    }

    public void writeConsultAndShowMoney(){
        if(consults.size() > 0){
            double money = 0;
            double totalMoney = 0;
            System.out.format("%s\n", "=".repeat(105));
            System.out.println("Consulten lijst");
            System.out.format("%s\n", "=".repeat(85));
            System.out.printf("| %-15s | %-15s | %-30s | %-12s |%n", "Specialism", "Rate Category", "Rate type", "Rate");
            System.out.format("%s\n", "=".repeat(85));
            for (Consult consult:consults){
                consult.write();
                money = consult.getRate();
                totalMoney = money + totalMoney;
            }
            System.out.println("Totaal bedrag: €" + String.format("%.2f",totalMoney));
            System.out.format("%s\n", "=".repeat(105));
        }else{
            System.out.println("Deze patiënt heeft op dit moment geen consulten gehad");
        }
    }

    public void writeAdd(Patient patient){
        System.out.println("Beschikbare consulten: ");
        for (Consult consult:consults){
            System.out.println(consult.getId() + " - " + consult.getSpecialism() + " - " + consult.getRateCategory() + " - " + consult.getRateType() + " €" + String.format("%.2f",consult.getRate()));
        }
        System.out.print("Voer #keuze in:");
        var scanner = new BScanner(System.in);
        int cons = scanner.nextInt();
        cons--;
        if (cons > consults.size() - 1){
            System.out.println("Voer een *geldig* cijfer in");
            writeAdd(patient);
        } else{
            consult = consults.get(cons);
            currentConsult = consult;
            System.out.println(currentConsult.getRateType() + " is toegevoegd aan de consulten lijst van " + patient.getFirstName() + " " + patient.getSurname());
            patient.addConsultToPatient(currentConsult);
        }
    }
}
