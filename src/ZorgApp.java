import java.util.ArrayList;

class ZorgApp {
   static ArrayList<User> users = new ArrayList<>();
   public static void main(String[] args) {
      User user0 = new Dentist(1, "Mart ElCamera", "Tandarts");
      User user1 = new PhysioTherapist(2, "Max Coorts", "Fysiotherapeut");
      User user2 = new Doctor(3, "Ferry Koelman", "Dokter");
      User user3 = new Dentist(4, "Sven Koslov", "Tandarts");
      User user4 = new Doctor(5, "Vera Amerongen", "Dokter");
      users.add(user0);
      users.add(user1);
      users.add(user2);
      users.add(user3);
      users.add(user4);

      boolean firstCycle = true;
      while (firstCycle) {
         checkInputUserInt();
      }
   }
   //Met deze functie toon je de gebruikers in een mooi rijtje. Vervolgens kan je een gebruikers id in typen
   //Hierdoor kan je een gebruiker selecteren. De functie checkt ook of je in gevoerde getal niet te groot is
   //voor een plek in de array, zodat de gebruiker een fout melding krijgt waar hij wat mee kan.
   public static void checkInputUserInt(){
      User user;
      User currentUser;
      var myObj = new BScanner(System.in);
      System.out.println("==================================================================");
      System.out.println("Selecteer uw gebruiker:");
      for (int i = 0; i < users.size(); i++) {
         user = users.get(i);
         System.out.println(user.getUserID() + " " + user.getUserName() + " (" + user.getDiscipline() + ")");
      }
      System.out.print( "Voer gebruikers ID in: " );
      int use = myObj.nextInt();
      //System.out.println("Here I am!!");
      use--;
      if(use > users.size() - 1){
         System.out.println( "Voer een *geldig* cijfer in" );
         checkInputUserInt();

      } else {
         //System.out.println("Here I am!!");
         currentUser = users.get(use);
         System.out.format("%s\n", "=".repeat(105));
         Administration administration = new Administration(currentUser);
         administration.menu(currentUser);
      }
   }
}
