import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

class Patient
{
   static final int RETURN      = 0;
   static final int SURNAME     = 1;
   static final int FIRSTNAME   = 2;
   static final int DATEOFBIRTH = 3;
   static final int WEIGHT      = 4;
   static final int LENGTH      = 5;




   private int       id;
   private String    surname;
   private String    firstName;
   private LocalDate dateOfBirth;
   private String    formattedDateOfBirth;

   private long age;
   private long days;
   private Integer ageInt;
   private LocalDate curDate;

   private double weight;
   private double length;
   private double bmi;
   private String bmiText;
   private MedicineList medicineList = new MedicineList();
   private ConsultList consultList = new ConsultList();
   private WeightList weightList = new WeightList();
   private LungContentList lungContentList = new LungContentList();

   static class FirstComparator implements Comparator<Patient> {
      @Override public int compare(Patient e1, Patient e2)
      {
         return (e1.surname + e1.firstName).compareTo(e2.surname + e2.firstName);
      }
   }

   // for sorting in descending order
   static class SecondComparator implements Comparator<Patient> {
      @Override public int compare(Patient e1, Patient e2)
      {
         return -(e1.surname + e1.firstName).compareTo(e2.surname + e2.firstName);
      }
   }
   static class ThirdComparator implements Comparator<Patient>{
      @Override public int compare(Patient e1, Patient e2)
      {
         if (e1.ageInt > e2.ageInt) {
            return -1;
         }
         else if (e1.ageInt < e2.ageInt) {
            return 1;
         }
         else {
            return (e1.ageInt).compareTo(e2.ageInt);
         }
      }
   }
   static class FourthComparator implements Comparator<Patient>{
      @Override public int compare(Patient e1, Patient e2)
      {
         if (e1.ageInt < e2.ageInt) {
            return -1;
         }
         else if (e1.ageInt > e2.ageInt) {
            return 1;
         }
         else {
            return 0;
         }
      }
   }
   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public int getId() {
      return id;
   }
   public String getSurname()
   {
      return surname;
   }

   public ConsultList getConsultList() {
      return consultList;
   }

   public LungContentList getLungContentList(){
      return lungContentList;
   }

   public MedicineList getMedicineList() {
      return medicineList;
   }
   public int getAgeInt(){ return ageInt;}
   public int setAgeInt(){ return this.ageInt = (int) days;}

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public String getFirstName()
   {
      return firstName;
   }
   public double getWeight() {
      return weight;
   }
   public void setSurname(String surname) {this.surname = surname;}

   public void setFirstname(String firstname) {
      this.firstName = firstname;
   }

   public void setDateOfBirth(String dateofbirth) {
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      this.dateOfBirth = LocalDate.from(dtf.parse(dateofbirth));
   }



   public void setWeight(double weight) {
      this.weight = weight;
   }

   public void setLength(double length) {
      this.length = length;
   }



   ////////////////////////////////////////////////////////////////////////////////
   // Constructor
   ////////////////////////////////////////////////////////////////////////////////
   public Patient( int id, String surname, String firstName, LocalDate dateOfBirth, double weight, double length )
   {
      this.id          = id;
      this.surname     = surname;
      this.firstName   = firstName;
      this.dateOfBirth = dateOfBirth;
      this.weight      = weight;
      //this.weight      = weightList.getCurrentweight();
      this.length      = length;
      curDate          = LocalDate.now();
      age              = calcAge();
      bmi              = calcBMI();
      setTextBMI(bmi);
      showDateOfBirth(this.dateOfBirth);

      calcDays();
      setAgeInt();
   }
   public void addMedicine(Medicine medicine) {
      medicineList.addMedicine(medicine);
   }

   public void addWeight(Weight weight){weightList.addWeight(weight);}
   public void showDateOfBirth(LocalDate dateOfBirth){
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
      formattedDateOfBirth = dateOfBirth.format(formatter);
   }

   ////////////////////////////////////////////////////////////////////////////////
   // Display patient data.
   ////////////////////////////////////////////////////////////////////////////////
   public void viewData(User user)
   {
      System.out.format( "===== Patiënt id=%d ==============================\n", id );
      System.out.format( "%-17s %s\n", "Achternaam:", surname );
      System.out.format( "%-17s %s\n", "Voornaam:", firstName );
      System.out.format( "%-17s %s\n", "Geboorte datum:", formattedDateOfBirth );
      System.out.format( "%-17s %s\n", "Leeftijd:", age );
      if(user.hasAccesBMI()){
         weightList.printCurrentWeight();
         System.out.format( "%-17s %s\n", "Lengte:", length + " m");
         System.out.format( "%-17s %.2f\n", "Bmi:", bmi );
         System.out.format( "%-17s %s\n", "Bmi status:", bmiText );

         //lungContentList.starLine();

      }


   }
   ///FUNCTIONS
   /////////////////////////
   ////////////////////////
   //Calculation of age
   /////////////////////
   public long calcAge() {
      return ChronoUnit.YEARS.between(this.dateOfBirth, curDate);
   }

   public long getDays() {
      return days;
   }

   public long calcDays(){return this.days =  ChronoUnit.DAYS.between(this.dateOfBirth, curDate);}

   //Calculation of bmi
   ////////////////////////////
   public double calcBMI() {
      return weight / (this.length * this.length);
   }

   public void medicineWrite() {
      medicineList.writeWithNumbers();
   }
   public void writeMedicine(){
      medicineList.write();
   }
   public void lungContentWrite(){lungContentList.starLine();}
   public void consultWrite(){
      consultList.writeConsultAndShowMoney();
   }
   public void addConsultToPatient(Consult consult){
      consultList.addConsult(consult);
   }
   public void addLungContentToPatient(LungContent lungContent){lungContentList.addLungContent(lungContent);}
   public void wait(int s)
   {
      try
      {
         TimeUnit.SECONDS.sleep(s);
      }
      catch(InterruptedException ex)
      {
         Thread.currentThread().interrupt();
      }
   }

   //Set the text message of the bmi
   //////////////////////////////////
   public void setTextBMI(double boMaIn) {
      this.bmi = boMaIn;
      if (this.age >= 70) {
         if (boMaIn <= 21.9) {
            bmiText = "Deze persoon heeft een *slanke* bmi status";
         } else if (boMaIn <= 28) {
            bmiText = "Deze persoon heeft een *goede* bmi-status";
         } else if (boMaIn <= 29.9) {
            bmiText = "Deze persoon heeft een *zware* bmi status";
         } else {
            bmiText = "Deze persoon heeft een *te zware* bmi status";
         }
      } else {
         if (boMaIn <= 16.9) {
            bmiText = "Deze persoon heeft een *te slanke* bmi status";
         } else if (boMaIn <= 18.4) {
            bmiText = "Deze persoon heeft een *slanke* bmi status";
         } else if (boMaIn <= 25) {
            bmiText = "Deze persoon heeft een *goede* bmi-status";
         } else if (boMaIn <= 29.9) {
            bmiText = "Deze persoon heeft een *zware* bmi status";
         } else {
            bmiText = "Deze persoon heeft een *te zware* bmi status";
         }
      }
   }

   //Edit data van een patient
   public void editData(Patient patient) {
      var scanner = new BScanner(System.in);
      boolean nextCycle2 = true;
      while (nextCycle2) {
         Patient currentPatient;
         currentPatient = patient;
         System.out.format("%s\n", "=".repeat(105));
         System.out.println(currentPatient.fullName());
         System.out.format("%s\n", "=".repeat(105));
         System.out.println("Selecteer een object dat u wilt wijzigen uit de onderstaande lijst ");
         System.out.format("%d:  Ga terug naar het hoofdmenu\n", RETURN);
         System.out.format("%d:  Verander de achternaam\n", SURNAME);
         System.out.format("%d:  Verander de voornaam\n", FIRSTNAME);
         System.out.format("%d:  Verander de geboorte datum\n", DATEOFBIRTH);
         System.out.format("%d:  Verander het gewicht\n", WEIGHT);
         System.out.format("%d:  Verander de lengte\n", LENGTH);

         System.out.print("Voer #keuze in: ");
         int change = scanner.nextInt();
         switch (change) {

            case RETURN:
               nextCycle2 = false;
               break;

            case SURNAME:
               //You choose to change the surname
               System.out.print("Voer een nieuwe achternaam in: ");
               var editScannerSurname = new BScanner(System.in);
               setSurname(editScannerSurname.nextLine());
               System.out.println("U heeft de achternaam veranderd naar " + getSurname());
               wait(10);
               break;

            case FIRSTNAME:
               //You choose to change the firstname
               System.out.print("Voer een nieuwe voornaam in: ");
               var editScannerFirstname = new BScanner(System.in);
               setFirstname(editScannerFirstname.nextLine());
               System.out.println("U heeft de voornaam veranderd naar " + getFirstName());
               wait(10);
               break;

            case DATEOFBIRTH:
               //You choose to change the date of birth
               System.out.print("Voer nieuwe geboorte datum in (jjjj-mm-dd): ");
               var editScannerDateOfBirth = new BScanner(System.in);
               setDateOfBirth(editScannerDateOfBirth.nextLine());
               this.age = calcAge();
               checkDateOfBirth();
               wait(10);
               break;

            case WEIGHT:
               // you choose to change the weight
               System.out.print("Voer nieuw gewicht in, in kilo's (00.0): ");
               var editScannerWeigth = new BScanner(System.in);
               setWeight(editScannerWeigth.nextDouble());
               addWeight(new Weight(getWeight()));
               this.bmi = calcBMI();
               setTextBMI(bmi);
               System.out.println("U hebt het gewicht veranderd en het bmi is opnieuw berekent.");
               wait(10);
               break;

            case LENGTH:
               // you choose to change the length
               System.out.println("Voer nieuwe lengte in, in meters (0.00):");
               var editScannerLength = new BScanner(System.in);
               setLength(editScannerLength.nextDouble());
               this.bmi = calcBMI();
               setTextBMI(bmi);
               System.out.println("U hebt de lengte veranderd en het bmi is opnieuw berekent.");
               wait(10);
               break;

            default:
               System.out.println("Voer een *geldig* cijfer in");
               break;
         }
      }
   }

   //Check date of birth format
   public void checkDateOfBirth() {
      if (this.age < 0) {
         var editDOBScanner = new BScanner(System.in);
         System.out.print("Voer een datum in die in het verleden ligt (jjjj-mm-dd): ");
         setDateOfBirth(editDOBScanner.nextLine());
         this.age = calcAge();
         checkDateOfBirth();
      } else {
         System.out.println("U heeft de geboorte datum gewijzigd en de leeftijd is opnieuw berekent.");
      }
   }


   ////////////////////////////////////////////////////////////////////////////////
   // Shorthand for a Patient's full name
   ////////////////////////////////////////////////////////////////////////////////
   public String fullName()
   {
      return String.format( "%s %s [%s]", firstName, surname, formattedDateOfBirth );
   }
}
