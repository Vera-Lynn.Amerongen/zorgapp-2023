import java.util.ArrayList;

public class WeightList {
    public ArrayList<Weight> weights = new ArrayList<>();
    private double currentweight;
    private Weight weight;
    private double lastWeight;
    private double difference;

    public WeightList(){

    }

    public double getCurrentweight() {
        return currentweight = weight.getWeight();
    }

    public void starLine(){
        lastWeight = 0;
        for(int i = 0; i < weights.size(); i++){
            currentweight = weights.get(i).getWeight();
            if( lastWeight > 0 ){
                if(lastWeight > currentweight ){
                    difference = lastWeight - currentweight;
                    if(difference >= 0.01 && difference <= 0.99 ){
                        weight.printWithStarsOneStarLes(currentweight);
                        lastWeight = currentweight;
                    } else if (difference >= 1 && difference <= 9) {
                        weight.printWithStarsTwoStarLes(currentweight);
                        lastWeight = currentweight;
                    } else {
                        weight.printWithStarsThreeStarLes(currentweight);
                        lastWeight = currentweight;
                    }
                } else if (lastWeight < currentweight) {
                    difference = lastWeight - currentweight;
                    if(difference >= 0.01 && difference <= 0.99 ){
                        weight.printWithStarsOneStarMore(currentweight);
                        lastWeight = currentweight;
                    } else if (difference >= 1 && difference <= 9) {
                        weight.printWithStarsTwoStarMore(currentweight);
                        lastWeight = currentweight;
                    } else {
                        weight.printWithStarsThreeStarMore(currentweight);
                        lastWeight = currentweight;
                    }
                }
            } else{
                weight.printWithStars(currentweight);
                lastWeight = currentweight;
            }
        }
        System.out.format( "%-1s %s\n","     kg |", "_".repeat(85));
        System.out.println("         Ster getal");
    }
    public void addWeight(Weight weight){
        weights.add(weight);
    }

    public void printCurrentWeight(){
        currentweight = weights.get(weights.size() -1).getWeight();
        weight = new Weight(currentweight);
        weight.write();

    }

    public void write(){
        for(Weight weight:weights){
            weight.write();
        }
    }

}
