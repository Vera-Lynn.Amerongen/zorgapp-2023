import java.util.concurrent.TimeUnit;

public class Consult {
    private int id;
    private String specialism;
    private String rateCategory;
    private String rateType;
    private double rate;

    public int getId() {
        return id;
    }

    public String getSpecialism() {
        return specialism;
    }

    public String getRateCategory() {
        return rateCategory;
    }

    public String getRateType() {
        return rateType;
    }

    public double getRate() {
        return rate;
    }


    public Consult(int id, String specialism, String rateCategory, String rateType, double rate){
        this.id             = id;
        this.specialism     = specialism;
        this.rateCategory   = rateCategory;
        this.rateType       = rateType;
        this.rate           = rate;
    }

    public void write(){
        System.out.printf("| %-15s | %-15s | %-30s | € %-10.2f |%n", getSpecialism(), getRateCategory(), getRateType(), getRate());
        System.out.format("%s\n", "-".repeat(85));

    }



}
