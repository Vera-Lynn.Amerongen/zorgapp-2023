import java.util.concurrent.TimeUnit;

public class Medicine {
    private static final int RETURN = 0;
    private static final int DOSSIS = 1;
    private final int id;
    private final String name;
    private final String type;
    private String dosis;

    public Medicine(int id, String name, String type, String dosis){
       this.id      = id;
       this.name    = name;
       this.type    = type;
       this.dosis   = dosis;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public String getType() {
        return type;
    }

    public void write(){
        System.out.format("===== Medicijn id=%d ==============================\n", getId());
        System.out.format("%-17s %s\n", "Naam:", getName());
        System.out.format("%-17s %s\n", "Soort:", getType());
        System.out.format("%-17s %s\n", "Dosis:", getDosis());
    }
    public void wait(int s)
    {
        try
        {
            TimeUnit.SECONDS.sleep(s);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }
    public void editMedicine(){
        var scanner = new BScanner(System.in);
        boolean nextCycle2 = true;
        while (nextCycle2) {
            System.out.format("%s\n", "=".repeat(105));
            System.out.println("Selecteer een object dat u wilt veranderen uit onderstaande lijst ");

            System.out.format("%d:  Ga terug naar het hoofd menu\n", RETURN);
            System.out.format("%d:  Verander de dosis\n", DOSSIS);

            System.out.print("Voer #keuze in: ");
            int change = scanner.nextInt();
            switch (change) {

                case RETURN:
                    nextCycle2 = false;
                    break;

                case DOSSIS:
                    //You choose to change the dose
                    System.out.print("Voer nieuwe dosis: ");
                    var editScannerDosis = new BScanner(System.in);
                    setDosis(editScannerDosis.nextLine());
                    System.out.println("Dosis is aangepast naar: "+ getName() + " " + getDosis());
                    wait(10);
                    break;


                default:
                    System.out.println("Voer een *geldig* cijfer in");
                    break;
            }
        }
    }


}
