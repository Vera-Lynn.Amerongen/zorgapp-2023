import java.time.LocalDate;
import java.util.ArrayList;

public class LungContentList {
    private double currentLC;
    private LocalDate currentDate;
    private double lastLC;
    private int lastStarCount;
    private double difference;
    private LungContent lc;
    public ArrayList<LungContent> lungContentArrayList = new ArrayList<>();

    public LungContentList(){

    }
    public void addLungContent(LungContent lungContent){
        lungContentArrayList.add(lungContent);
    }

    public void starLine(){
        lastLC = 0;
        for (int i = 0; i < lungContentArrayList.size(); i++){
            currentLC = lungContentArrayList.get(i).getLungContentNumber();
            currentDate = lungContentArrayList.get(i).getDate();
            lc = new LungContent (currentDate,currentLC);
            if( lastLC > 0 ){
                if(lastLC > currentLC ){
                    difference = lastLC - currentLC;
                    if(difference >= 0.01 && difference <= 0.50 ){
                        lc.printWithStarsOneStarLes(currentDate,currentLC, lastStarCount);
                        lastLC = currentLC;
                        lastStarCount = lc.getStarLength();
                    } else if (difference >= 0.51 && difference <= 1) {
                        lc.printWithStarsTwoStarLes(currentDate,currentLC, lastStarCount);
                        lastLC = currentLC;
                        lastStarCount = lc.getStarLength();
                    } else {
                        lc.printWithStarsThreeStarLes(currentDate,currentLC, lastStarCount);
                        lastLC = currentLC;
                        lastStarCount = lc.getStarLength();
                    }
                } else if (lastLC < currentLC) {
                    difference = lastLC - currentLC;
                    if(difference >= 0.01 && difference <= 0.50 ){
                        lc.printWithStarsOneStarMore(currentDate,currentLC, lastStarCount);
                        lastLC = currentLC;
                        lastStarCount = lc.getStarLength();
                    } else if (difference >= 0.51 && difference <= 1) {
                        lc.printWithStarsTwoStarMore(currentDate,currentLC, lastStarCount);
                        lastLC = currentLC;
                        lastStarCount = lc.getStarLength();
                    } else {
                        lc.printWithStarsThreeStarMore(currentDate,currentLC, lastStarCount);
                        lastLC = currentLC;
                        lastStarCount = lc.getStarLength();
                    }
                } else if (lastLC == currentLC){
                    lc.printWithStars(currentDate,currentLC);
                    lastLC = currentLC;
                    lastStarCount = lc.getStarLength();
                }
            } else{
                lc.printWithStars(currentDate,currentLC);
                lastLC = currentLC;
                lastStarCount = lc.getStarLength();
            }
        }
    }
}
